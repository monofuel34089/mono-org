provider "aws" {
    # monofuel34089@gmail.com user
    profile = "monofuel"
	region = var.aws_region
}

terraform {
	backend "s3" {
		bucket = "mono-org-terraform"
		key = "mono-org/tf.state"
		region = "us-east-1"
	}
}

resource "aws_s3_bucket" "b" {
	bucket = "mono-org-terraform"
 	acl    = "private"
}

resource "aws_organizations_organization" "mono_org" {
  aws_service_access_principals = [
    "cloudtrail.amazonaws.com",
    "config.amazonaws.com",
  ]

  feature_set = "ALL"
}

# master account
resource "aws_organizations_account" "master" {
  name  = "andrew brower"
  email = "monofuel34089@gmail.com"
}

# NB. have to do a password reset to get access to new users

# Account for AWS training examples
resource "aws_organizations_account" "aws_training" {
  name  = "AWS Training Account"
  email = "monofuel34089+aws_training@gmail.com"

  iam_user_access_to_billing = "ALLOW"
}
